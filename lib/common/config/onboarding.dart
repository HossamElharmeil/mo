/// the welcome screen data
/// set onBoardingData = [] if you would like to hide the onboarding
List onBoardingData = [
  {
    "title": "Welcome to Molly's Bakery",
    "image": "assets/images/fogg-delivery-1.jpg",
    "desc": "Molly's is on the way to serve you. "
  },
  {
    "title": "We make it, You bake it.",
    "image": "assets/images/fogg-uploading-1.jpg",
    "desc":
        "Molly’s Bakery provides you with unbaked, frozen bakery so you can bake them fresh in your own oven! It’s like having your own bakery shop in your home! "

  },
  {
    "title": "Let's Get Started",
    "image": "assets/images/fogg-uploading-1.jpg",
    "desc": "Waiting no more, let's see what you get!"
  },
];
