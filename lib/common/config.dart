export 'config/advertise.dart';
export 'config/general.dart';
export 'config/onboarding.dart';
export 'config/payments.dart';
export 'config/products.dart';
export 'config/smartchat.dart';

/// Server config demo for WooCommerce
/// Get more example for Opencart / Magento / Shopify from the example folder
const serverConfig = {
  "type": "woo",
  "url": "https://mollys-eg.com/",

  /// document: https://docs.inspireui.com/fluxstore/woocommerce-setup/
  "consumerKey": "ck_e542b36182062c025bc1ce945e748caa7f5cbe92",
  "consumerSecret": "cs_60dcf3dad25195381d8815de4745c69ef39db97b",

  /// Your website woocommerce. You can remove this line if it same url
  "blog": "https://tamere1.sg-host.com",

  /// set blank to use as native screen
  "forgetPassword": "https://tamere1.sg-host.com/wp-login.php?action=lostpassword"
};
