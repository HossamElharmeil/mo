import 'package:event_bus/event_bus.dart';
import 'package:universal_platform/universal_platform.dart';

/// enable network proxy
const debugNetworkProxy = false;

/// some constants Local Key
const kLocalKey = {
  "userInfo": "userInfo",
  "shippingAddress": "shippingAddress",
  "recentSearches": "recentSearches",
  "wishlist": "wishlist",
  "home": "home",
  "cart": "cart",
  "countries": "countries"
};

/// Logging config
const kLOG_TAG = "[Fluxstore]";
const kLOG_ENABLE = true;

void printLog(dynamic data) {
  if (kLOG_ENABLE) {
    // ignore: avoid_print
    print("[${DateTime.now().toUtc()}]$kLOG_TAG${data.toString()}");
  }
}

/// check if the environment is web
final bool kIsWeb = UniversalPlatform.isWeb;
final bool isIos = UniversalPlatform.isIOS;
final bool isAndroid = UniversalPlatform.isAndroid;
final bool isMacOS = UniversalPlatform.isMacOS;
final bool isWindow = UniversalPlatform.isWindows;
final bool isFuchsia = UniversalPlatform.isFuchsia;

/// use eventbus for fluxbuilder
final EventBus eventBus = EventBus();

/// constant for Magento payment
const kMagentoPayments = [
  "HyperPay_Amex",
  "HyperPay_ApplePay",
  "HyperPay_Mada",
  "HyperPay_Master",
  "HyperPay_PayPal",
  "HyperPay_SadadNcb",
  "HyperPay_Visa",
  "HyperPay_SadadPayware"
];

const ApiPageSize = 20;

/// Use for set default SMS Login
class LoginSMSConstants {
  static const String countryCodeDefault = 'VN';
  static const String dialCodeDefault = '+84';
  static const String nameDefault = 'Vietnam';
}
