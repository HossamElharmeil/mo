import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'models/cart/cart_base.dart';

import 'package:after_layout/after_layout.dart';
import 'package:provider/provider.dart';
import 'package:custom_splash/custom_splash.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:inspireui/utils/screen_utils.dart';
import 'common/config.dart';
import 'common/constants.dart';
import 'models/app.dart';
import 'models/blogs/blog.dart';
import 'models/category/category_model.dart';
import 'models/filter_attribute.dart';
import 'models/filter_tags.dart';
import 'screens/home/onboard_screen.dart';
import 'screens/users/login.dart';
import 'services/index.dart';
import 'widgets/common/animated_splash.dart';

class AppInit extends StatefulWidget {
  final Function onNext;

  AppInit({this.onNext});

  @override
  _AppInitState createState() => _AppInitState();
}

class _AppInitState extends State<AppInit> with AfterLayoutMixin {
  final StreamController<bool> _streamInit = StreamController<bool>();

  bool isFirstSeen = false;
  bool isLoggedIn = false;
  Map appConfig = {};

  /// check if the screen is already seen At the first time
  Future<bool> checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = prefs.getBool('seen') ?? false;
    return _seen;
  }

  /// Check if the App is Login
  Future checkLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('loggedIn') ?? false;
  }

  Future loadInitData() async {
    try {
      printLog("[AppState] Inital Data");
      isFirstSeen = await checkFirstSeen();
      setState(() {});

      isLoggedIn = await checkLogin();

      /// Load App model config
      Services().setAppConfig(serverConfig);
      appConfig =
      await Provider.of<AppModel>(context, listen: false).loadAppConfig();
      Provider.of<CartModel>(context, listen: false).changeCurrencyRates(
          Provider.of<AppModel>(context, listen: false).currencyRate);

      Future.delayed(Duration.zero, () {
        /// Load more Category/Blog/Attribute Model beforehand
        if (mounted) {
          Provider.of<CategoryModel>(context, listen: false).getCategories(
            lang: Provider.of<AppModel>(context, listen: false).locale,
            cats: Provider.of<AppModel>(context, listen: false).categories,
          );

          Provider.of<BlogModel>(context, listen: false).getBlogs();

          Provider.of<FilterTagModel>(context, listen: false).getFilterTags();

          Provider.of<FilterAttributeModel>(context, listen: false)
              .getFilterAttributes();
        }
      });

      printLog("[AppState] Init Data Finish");
    } catch (e, trace) {
      print(e.toString());
      print(trace.toString());
    }
    if (!_streamInit.isClosed) {
      _streamInit.add(isFirstSeen);
    }
  }

  Widget onNextScreen(bool isFirstSeen) {
    if (!isFirstSeen && !kIsWeb && appConfig != null) {
      if (onBoardingData.isNotEmpty) return OnBoardScreen(appConfig);
    }

    if (kLoginSetting['IsRequiredLogin'] && !isLoggedIn) {
      return LoginScreen(
        onLoginSuccess: (context) async {
          await Navigator.pushNamed(context, RouteList.home);
        },
      );
    }
    return widget.onNext();
  }

  @override
  void initState() {
    loadInitData();
    super.initState();
  }

  @override
  void dispose() {
    _streamInit?.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: _streamInit.stream,
      builder: (context, snapshot) {
        var _isPushNext = snapshot.data != null;

        if (kSplashScreen.lastIndexOf('flr') > 0) {
          return SplashScreen.navigate(
            name: kSplashScreen,
            startAnimation: 'fluxstore',
            backgroundColor: Colors.white,
            next: (ct) => onNextScreen(isFirstSeen),
            until: () => Future.delayed(Duration(seconds: 3)),
          );
        }

        if (kSplashScreen.lastIndexOf('png') > 0) {
          return AnimatedSplash(
            imagePath: kSplashScreen,
            home: onNextScreen(isFirstSeen),
            duration: 2000,
            type: AnimatedSplashType.StaticDuration,
            isPushNext: _isPushNext,
          );
        }

        return CustomSplash(
          imagePath: kLogoImage,
          backGroundColor: Colors.white,
          animationEffect: 'fade-in',
          logoSize: 50,
          home: onNextScreen(isFirstSeen),
          duration: 2500,
        );
      },
    );
  }

  @override
  void afterFirstLayout(BuildContext context) {
    ScreenUtil.init(context);
  }
}